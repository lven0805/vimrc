" 显示行号
set nu

" 语法高亮
syntax on 

" 历史行号
set history=1000

" vim自动对齐
set autoindent

" tab为4个空格
set tabstop=4

" 设置自动缩进
set ai!